package com.cdm.upv.practica_04;

 import android.content.Intent;
 import android.os.Bundle;
 import android.support.design.widget.FloatingActionButton;
 import android.support.design.widget.Snackbar;
 import android.support.v7.app.AppCompatActivity;
 import android.support.v7.widget.Toolbar;
 import android.view.Menu;
 import android.view.MenuItem;
 import android.view.View;
 import android.widget.Button;
 import android.widget.EditText;
 import android.widget.TextView;

 public class millas extends AppCompatActivity {

 Button bt;
 EditText et;
 TextView tv;

 @Override
 protected void onCreate(Bundle savedInstanceState) {
 super.onCreate(savedInstanceState);
 setContentView(R.layout.activity_millas);
 Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
 setSupportActionBar(toolbar);

 bt = (Button) findViewById(R.id.button2);
 et = (EditText) findViewById(R.id.editText2);
 tv = (TextView) findViewById(R.id.textView3);

 bt.setOnClickListener(new View.OnClickListener() {
 @Override
 public void onClick(View view) {
 String dato = et.getText().toString();
 double distancia = Double.parseDouble(dato);
 double kilometros = distancia * 1.609;
 tv.setText("La distancia en kilometros es:" + kilometros);
 }
 });
 }

 @Override
 public boolean onCreateOptionsMenu(Menu menu) {
 // Inflate the menu; this adds items to the action bar if it is present.
 getMenuInflater().inflate(R.menu.menu, menu);
 return true;
 }

 @Override
 public boolean onOptionsItemSelected(MenuItem item) {
 switch (item.getItemId()) {
 case R.id.action_settings:
 // User chose the "Settings" item, show the app settings UI...
 return true;

 case R.id.action_regresar:
 Intent i = new Intent(this, MainActivity.class);
 startActivity(i);
 return true;

 default:
 // If we got here, the user's action was not recognized.
 // Invoke the superclass to handle it.
 return super.onOptionsItemSelected(item);

 }

 }

 }
