package com.cdm.upv.practica_04;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
    import android.support.v7.app.AppCompatActivity;
    import android.support.v7.widget.Toolbar;


    public class area extends AppCompatActivity {

        Button bt;
        EditText et;
        TextView tv;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_area);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            bt = (Button) findViewById(R.id.button3);
            et = (EditText) findViewById(R.id.editText3);
            tv = (TextView) findViewById(R.id.textView6);

            bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String dato = et.getText().toString();
                    double lado = Double.parseDouble(dato);
                    double area = lado * lado;
                    tv.setText("El area del cuadrado es: " + area);
                }
            });

        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_settings:
                    // User chose the "Settings" item, show the app settings UI...
                    return true;

                case R.id.action_regresar:
                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                    return true;

                default:
                    // If we got here, the user's action was not recognized.
                    // Invoke the superclass to handle it.
                    return super.onOptionsItemSelected(item);

            }

        }
    }


